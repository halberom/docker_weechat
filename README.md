Example run

    $ docker run -it --rm \
      -e TERM -u $(id -u):$(id -g) \
      -v /etc/localtime:/etc/localtime:ro \
      -v $HOME/.weechat:/home/user/.weechat \
      --log-driver=none \
      --name weechat \
      halberom/weechat
